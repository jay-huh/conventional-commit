# Conventional Commits and Semantic Versioning

by jay.jyhuh@gmail.com

## About

- [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)
- [Semantic Versioning](https://semver.org/)
- [keep a changelog](https://keepachangelog.com/en/1.0.0/)

## Installation

  ```bash
  # Install nvm and npm
  sudo curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.37.2/install.sh | bash
  nvm install --lts

  npm install -g commitizen
  npm install -g cz-conventional-changelog

  npm install -g standard-version
  echo '{ "path": "cz-conventional-changelog" }' > ~/.czrc
  ```

## Usage

  ```bash
  # commitizen: Conventional commits
  git cz [-avs... : git commit options]

  # First Release( Create CHANGELOG.md and Tag )
  standard-version --first-release --release-as 0.0.1 [--dry-run]

  # Release: Semantic version and generate CHANGELOG
  standard-version [-p alpha|beta...]
  ```

- pre-release option: `-p [alpha|beta...]`
  - example: `standard-version -p alpha [--dry-run]`

## Customize commit types

> Default config is NOT `keep a changelog` style.

- file path about types
  `~/.nvm/versions/node/v14.15.5/lib/node_modules/commitizen/node_modules/conventional-commit-types/index.json`

## Customize default configuration

> Default config is NOT `keep a changelog` style.

- default config file
  `~/.nvm/versions/node/v14.15.5/lib/node_modules/standard-version/node_modules/conventional-changelog-config-spec/versions/2.1.0/schema.json`

### [Optional] Apply Pre-commit

- update `.git/hooks/prepare-commit-msg`

  ```bash
  #!/bin/bash

  exec < dev/tty && cz --hook || true
  ```

<!--
## Install `Commitizen`
  ```bash
  sudo pip3 install -U pre-commit
  sudo pip3 install -U Commitizen
  ```

## Commands

  - `cz bump -h`
 -->
