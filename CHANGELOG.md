# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.2](https://gitlab.com/jay-huh/bumpertest/compare/v0.0.1...v0.0.2) (2021-02-22)


### Bug Fixes

* **readme:** update README.md for usage ([83ed418](https://gitlab.com/jay-huh/bumpertest/commit/83ed41894232c83a9a73355be5f120b8eca954eb))

### 0.0.1 (2021-02-22)


### Features

* prepare for commitizen and standard-version ([e8dafc8](https://gitlab.com/jay-huh/bumpertest/commit/e8dafc82cfa393515014c3c8d3533def3378480c))


### Bug Fixes

* **readme:** update README ([ce77cf5](https://gitlab.com/jay-huh/bumpertest/commit/ce77cf520e578d905ff165b0adffe94751ce8223)), closes [#1](https://gitlab.com/jay-huh/bumpertest/issues/1)
* **readme:** update README and test issue and `MR` ([1c839fe](https://gitlab.com/jay-huh/bumpertest/commit/1c839fe6169c6f1b92a50a0316a2ca7175903f5b)), closes [#1](https://gitlab.com/jay-huh/bumpertest/issues/1)
